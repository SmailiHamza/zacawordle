package com.zenika.academy.barbajavas.wordle.domain.model;

public class User {
    private String name;
    private String email;
    private String tId;

    public User() {

    }

    public User(String email, String name) {
        this.name = name;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getTId() {
        return tId;
    }

    public void settId(String tId) {
        this.tId = tId;
    }
}
