package com.zenika.academy.barbajavas.wordle.configuration;

import com.zenika.academy.barbajavas.wordle.domain.service.i18n.I18n;
import com.zenika.academy.barbajavas.wordle.domain.service.i18n.I18nFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanPrinterConfig {
    I18n i18n;
    @Bean
    public I18n i18n (@Value("${wordle-arg.language}") String language) throws Exception {
        try {
            // Get i18n resources
            return I18nFactory.getI18n(language);
        } catch (Exception e) {
            throw new Exception("Can't initialize app.\nBe sure to add arg with language among 'FR' or 'EN'");
        }

    }
}
