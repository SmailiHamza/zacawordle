package com.zenika.academy.barbajavas.wordle.domain.service;

public class UnauthorizedException extends Exception{
    private String message;
    public UnauthorizedException(String message) {
        super();
        this.message=message;
    }
    @Override
    public String getMessage(){
        return this.message;
    }
}
