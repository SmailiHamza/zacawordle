package com.zenika.academy.barbajavas.wordle.application;

import com.zenika.academy.barbajavas.wordle.domain.model.Game;
import com.zenika.academy.barbajavas.wordle.domain.repository.GameRepository;
import com.zenika.academy.barbajavas.wordle.domain.repository.UserRepository;
import com.zenika.academy.barbajavas.wordle.domain.service.BadLengthException;
import com.zenika.academy.barbajavas.wordle.domain.service.DictionaryService;
import com.zenika.academy.barbajavas.wordle.domain.service.IllegalWordException;
import com.zenika.academy.barbajavas.wordle.domain.service.UnauthorizedException;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
public class GameManager {

    private final DictionaryService dictionaryService;
    private final GameRepository gameRepository;
    private final UserRepository userRepository;

    public GameManager(DictionaryService dictionaryService, GameRepository gameRepository, UserRepository userRepository) {
        this.dictionaryService = dictionaryService;
        this.gameRepository = gameRepository;
        this.userRepository = userRepository;
    }

    public Game startNewGame(int wordLength, int nbAttempts, String userTid) {
        if (!userTid.equals("")) {
            userRepository.findByTid(userTid).orElseThrow(() -> new IllegalArgumentException("User not found"));
        }
        Game game = new Game(UUID.randomUUID().toString(), userTid, dictionaryService.getRandomWord(wordLength), nbAttempts);
        gameRepository.save(game);
        return game;
    }

    public Game attempt(String gameTid, String word, String userTid) throws IllegalWordException, BadLengthException, UnauthorizedException {

        Game game = gameRepository.findByTid(gameTid)
                .orElseThrow(() -> new IllegalArgumentException("This game does not exist"));
        if (game.getUserTid().equals(userTid)) {
            if (!dictionaryService.wordExists(word)) {
                throw new IllegalWordException();
            }
            if (word.length() != game.getWordLength()) {
                throw new BadLengthException();
            }

            game.guess(word);

            gameRepository.save(game);
        } else {
            throw new UnauthorizedException("Action not allowed");
        }
        return game;
    }

    public Game getGameById(String gameTid) {
        return gameRepository.findByTid(gameTid)
                .orElseThrow(() -> new IllegalArgumentException("This game does not exist"));
    }

    public List<Game> findByUserTid(String userTid) {
        return gameRepository.findByUserTid(userTid);
    }
}
