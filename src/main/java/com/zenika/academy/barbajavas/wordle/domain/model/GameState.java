package com.zenika.academy.barbajavas.wordle.domain.model;

import org.springframework.stereotype.Component;


public enum GameState {
    WIN, LOSS, IN_PROGRESS
}
