package com.zenika.academy.barbajavas.wordle.domain.repository;

import com.zenika.academy.barbajavas.wordle.domain.model.Game;
import com.zenika.academy.barbajavas.wordle.domain.model.User;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class GameRepository {
    private Map<String, Game> games = new HashMap<>();

    public void save(Game game) {
        this.games.put(game.getTid(), game);
    }

    public Optional<Game> findByTid(String tid) {
        return Optional.ofNullable(games.get(tid));
    }

    public List<Game> findByUserTid(String userTid) {
        List<Game> gamesList = new ArrayList<>();
        games.forEach((key, game) -> {
            if (game.getUserTid().equals(userTid)) {
                gamesList.add(game);
            }
        });
        return gamesList;
    }
}
