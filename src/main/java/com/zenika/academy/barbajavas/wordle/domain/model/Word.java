package com.zenika.academy.barbajavas.wordle.domain.model;

public class Word {
    private boolean wordExists;
    private String word;
    public Word(boolean wordExists,String word){
        this.word=word;
        this.wordExists=wordExists;
    }
    public Word(){
    }

    public boolean getWordExists() {
        return wordExists;
    }

    public String getWord() {
        return word;
    }


}
