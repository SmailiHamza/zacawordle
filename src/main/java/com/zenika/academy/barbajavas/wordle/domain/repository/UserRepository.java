package com.zenika.academy.barbajavas.wordle.domain.repository;

import com.zenika.academy.barbajavas.wordle.domain.model.User;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class UserRepository {
    private Map<String, User> users = new HashMap<>();

    public User save(User user, String tId) {
        user.settId(tId);
        this.users.put(user.getTId(), user);
        return this.users.get(user.getTId());
    }

    public List<User> findByName(String name) {
        List<User> listUsers = new ArrayList<>();
        users.forEach((key, user) -> {
            if (user.getName().equals(name)) {
                listUsers.add(user);
            }
        });
        return listUsers;
    }

    public Optional<User> findByEmail(String email) {
        Optional<User> user = users.values().stream().filter(u -> email.equals(u.getEmail())).findFirst();
        return user;
    }

    public Optional<User> findByTid(String Tid) {
        Optional<User> user = users.values().stream().filter(u -> users.get(Tid) != null).findFirst();
        return user;
    }

    public User updateUser(String email, String name) {
        String key = "";
        for (Map.Entry u : users.entrySet()) {
            if (users.get(u.getKey()).getEmail().equals(email)) {
                User user = new User(email, name);
                key = (String) u.getKey();
                user.settId(key);
                users.put((String) u.getKey(), user);
            }
        }
        return users.get(key);
    }

    public String removeUser(String email) {
        String key = "";
        for (Map.Entry u : users.entrySet()) {
            if (users.get(u.getKey()).getEmail().equals(email)) {
                key = (String) u.getKey();
            }
        }
        users.remove(key);
        return "User removed !";
    }
}
