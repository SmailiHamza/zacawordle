package com.zenika.academy.barbajavas.wordle.application;

import com.zenika.academy.barbajavas.wordle.domain.model.User;
import com.zenika.academy.barbajavas.wordle.domain.repository.UserRepository;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserManager {
    private final UserRepository userRepository;

    public UserManager(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User save(User user,String tId) {
        userRepository.findByEmail(user.getEmail()).ifPresentOrElse((u) -> {throw new IllegalArgumentException("This email is already exist");},() -> userRepository.save(user,tId));
        return user;
    }
    public List<User> findByNameOrEmail(String email,String name) {
        List<User> userList=new ArrayList<>();
        if(!email.equals("")){
           userList.add(findByEmail(email));
        }else if(!name.equals("")){
            userList=findByName(name);
        }
        return userList;
    }
    public List<User> findByName(String name) {
        return userRepository.findByName(name);
    }

    public User findByEmail(String email) {
         return userRepository.findByEmail(email).orElseThrow(()-> new IllegalArgumentException("This email does not exist"));
    }

    public User updateUser(String email, String name) {
        userRepository.findByEmail(email).orElseThrow(()-> new IllegalArgumentException("This email does not exist"));
        return userRepository.updateUser(email, name);
    }

    public String removeUser(String email) {
        userRepository.findByEmail(email).orElseThrow(()-> new IllegalArgumentException("This email does not exist"));
        return userRepository.removeUser(email);
    }
}
