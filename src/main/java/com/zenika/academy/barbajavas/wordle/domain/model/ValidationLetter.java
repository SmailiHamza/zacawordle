package com.zenika.academy.barbajavas.wordle.domain.model;

import org.springframework.stereotype.Component;


public enum ValidationLetter {
    GOOD_POSITION,
    WRONG_POSITION,
    NOT_IN_WORD
}
