package com.zenika.academy.barbajavas.wordle.domain.service.i18n;

import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.Normalizer;
import java.util.Locale;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.Scanner;

public class Fr extends AbstractI18n {

    public Fr() throws FileNotFoundException {
        super(ResourceBundle.getBundle("messages", new Locale("fr", "FR")));
    }

    @Override
    protected void loadDictionnary() throws FileNotFoundException {
        try {
            FileReader fileDicoFR = new FileReader("resources/dico_fr.txt");

            BufferedReader bufferDicoFr = new BufferedReader(fileDicoFR);
             // Holds true till there is nothing to read
            String toCheckLength;
            while (bufferDicoFr.ready()) {
                // Print all the content of a file
                String line = bufferDicoFr.readLine();
                if (line.length() > 3 && !line.contains("-") && line.length() < 11) {
                    this.dictionary.add(this.removeAccent(line).toUpperCase());
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private String removeAccent(String word) {
        String regex = "[^\\p{ASCII}]";
        return Normalizer.normalize(word, Normalizer.Form.NFD).replaceAll(regex, "");
    }
}