package com.zenika.academy.barbajavas.wordle.domain.service;

import com.zenika.academy.barbajavas.wordle.domain.model.Word;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@ConditionalOnProperty(value = "wordle-arg.source", havingValue = "api")
public class ApiDictService implements DictionaryService {
    @Value("${wordle-arg.uri}")
    String scrabbleApiUrl;
    @Override
    public String getRandomWord(int length) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Word> responseEntity =
                restTemplate.getForEntity("http://"+scrabbleApiUrl+"/api/dictionaries/fr/randomWord?length=" + length, Word.class);
        Word word = responseEntity.getBody();
        return word.getWord();
    }

    @Override
    public boolean wordExists(String wordguess) throws IllegalWordException {
        RestTemplate restTemplate = new RestTemplate();
        try {
            ResponseEntity<Word> responseEntity =
                    restTemplate.getForEntity("http://"+scrabbleApiUrl+"/api/dictionaries/fr/words/" + wordguess, Word.class);
            Word word = responseEntity.getBody();
            return word.getWordExists();
        } catch (Exception illegalWordException) {
            throw new IllegalWordException();
        }


    }
}
