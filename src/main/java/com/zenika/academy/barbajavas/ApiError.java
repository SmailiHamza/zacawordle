package com.zenika.academy.barbajavas;

import org.springframework.http.HttpStatus;


public class ApiError {

    private HttpStatus status;
    private String message;
    private int statusCode;

    public ApiError(HttpStatus status, String message) {
        this.status = status;
        this.message = message;
        this.statusCode =status.value();
    }

    public String getMessage() {
        return message;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public int getStatusCode() {
        return statusCode;
    }
}