package com.zenika.academy.barbajavas;

import com.zenika.academy.barbajavas.wordle.application.GameManager;
import com.zenika.academy.barbajavas.wordle.domain.model.Game;
import com.zenika.academy.barbajavas.wordle.domain.service.BadLengthException;
import com.zenika.academy.barbajavas.wordle.domain.service.IllegalWordException;
import com.zenika.academy.barbajavas.wordle.domain.service.UnauthorizedException;
import com.zenika.academy.barbajavas.wordle.domain.service.i18n.I18n;
import com.zenika.academy.barbajavas.wordle.domain.service.i18n.I18nFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("api")

public class GameController {
    GameManager gameManager;
    I18n i18n;
    Game game;

    @Autowired
    public GameController(GameManager gameManager) throws Exception {
        this.gameManager = gameManager;
        this.i18n = I18nFactory.getI18n("FR");
    }

    @PostMapping("game")
    public Game createGame(@RequestParam(value = "wordLength") int wordLength, @RequestParam(value = "nbAttempts") int nbAttempts, @RequestParam(value = "userTid", defaultValue = "") String userTid) {
        return game = gameManager.startNewGame(wordLength, nbAttempts, userTid);
    }

    @PostMapping("game/{gameTid}")
    public Game playRound(@PathVariable(value = "gameTid") String gameTid, @RequestBody Map<String, String> body, @RequestHeader("x-userTid") String userTid) throws BadLengthException, IllegalWordException, UnauthorizedException {
        return game = gameManager.attempt(gameTid, body.get("guess"), userTid);

    }

    @GetMapping("game/{gameTid}")
    public Game getGame(@PathVariable(value = "gameTid") String gameTid) {
        return gameManager.getGameById(gameTid);
    }

    @GetMapping("game/user/{userTid}")
    public List<Game> getListGame(@PathVariable(value = "userTid") String userTid) {
        return gameManager.findByUserTid(userTid);
    }

    @ExceptionHandler(BadLengthException.class)
    public ResponseEntity<ApiError> handleBadLengthException(BadLengthException cnae) {
        return new ResponseEntity<>(new ApiError(HttpStatus.BAD_REQUEST, i18n.getMessage(cnae.getMessage(), game.getWordLength())), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(IllegalWordException.class)
    public ResponseEntity<Object> handleIllegalWordException(IllegalWordException cnae) {
        return new ResponseEntity<>(new ApiError(HttpStatus.BAD_REQUEST, i18n.getMessage(cnae.getMessage())), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Object> handleIllegalArgumentException(IllegalArgumentException cnae) {
        return new ResponseEntity<>(new ApiError(HttpStatus.BAD_REQUEST, i18n.getMessage(cnae.getMessage())), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UnauthorizedException.class)
    public ResponseEntity<Object> handleUnauthorizedException(UnauthorizedException cnae) {
        return new ResponseEntity<>(new ApiError(HttpStatus.UNAUTHORIZED, i18n.getMessage(cnae.getMessage())), HttpStatus.UNAUTHORIZED);
    }


}
