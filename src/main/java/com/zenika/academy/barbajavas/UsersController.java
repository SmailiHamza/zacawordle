package com.zenika.academy.barbajavas;

import com.zenika.academy.barbajavas.wordle.application.UserManager;
import com.zenika.academy.barbajavas.wordle.domain.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

@RestController
@RequestMapping("api")

public class UsersController {
    UserManager userManager;
    private final AtomicLong counter = new AtomicLong();

    @Autowired
    public UsersController(UserManager userManager) {
        this.userManager = userManager;
    }

    @GetMapping("users")
    public List<User> getUserByEmail(@RequestParam(value = "email", defaultValue = "") String email, @RequestParam(value = "name", defaultValue = "") String name) {
        return userManager.findByNameOrEmail(email, name);
    }

    @PostMapping("users")
    public User createUser(@RequestBody User user) {
        return userManager.save(user, UUID.randomUUID().toString());
    }

    @DeleteMapping("users")
    public String removeUser(@RequestParam(value = "email") String email) {
        return userManager.removeUser(email);
    }

    @PutMapping("users")
    public User updateUser(@RequestBody User user) {
        return userManager.updateUser(user.getEmail(), user.getName());
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<ApiError> handleIllegalArgumentException(IllegalArgumentException cnae) {
        return new ResponseEntity<>(new ApiError(HttpStatus.BAD_REQUEST, cnae.getMessage()), HttpStatus.BAD_REQUEST);
    }
}
